//
//  ViewModelCat.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import Foundation
final class CatViewModel: ObservableObject{
    
    @Published var catList: [Cat] = []
    
    init() {
        self.getCats()
    }
    
    func getCats(){
        APICaller.shared.getListCats() {
            cats in
            self.catList = cats
        }
    }
    
    

}


//
//  Cat.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import Foundation

struct Cat: Identifiable, Hashable, Codable{
    var id: String
    var name:String
    var origin: String
    var affection_level: Int
    var intelligence: Int
    var reference_image_id: String?
}

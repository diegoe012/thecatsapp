//
//  API.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import Foundation

struct Constants {
    static let API_KEY = "bda53789-d59e-46cd-9bc4-2936630fde39"
    static let baseURL = "https://api.thecatapi.com/v1/breeds"
}


class APICaller{
    static let shared = APICaller()
    
    func getListCats(completion: @escaping ([Cat]) -> Void){
        guard let url = URL(string: "\(Constants.baseURL)") else {return}
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)){
            data, _, error in guard let data,error == nil else{
                return
            }
            do {
                let results = try JSONDecoder().decode([Cat].self, from: data)
                completion(results)
                    
            } catch{
                debugPrint(error)
            }
        }
        task.resume()
    }
    
}



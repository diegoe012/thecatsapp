//
//  CatView.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import SwiftUI

struct CatCard: View {
    let cat: Cat
    let padding:CGFloat = CGFloat(35)

    var body: some View {
        
        VStack(alignment: .leading){
            Text(cat.name).font(.title3)
                .padding(.leading, padding)
            Divider()
        }
        
        VStack {
            
            AsyncImage(url: URL(string: "https://cdn2.thecatapi.com/images/\(cat.reference_image_id ?? "").jpg")) { image in
                image.resizable()
            } placeholder: {
                ProgressView()
            }
            .frame(width: 320, height: 320)
            .clipShape(RoundedRectangle(cornerRadius: 10))
            
            HStack {
                VStack(alignment: .leading) {
                    HStack {
                        Text("Origin: ").bold()
                        Text(cat.origin)
                    }.padding(.leading, padding)
                }
                Spacer()
                VStack(alignment: .trailing) {
                    HStack {
                        Text("Intelligence: ").bold()
                        Text(String(format: "%d", cat.intelligence))
                    }.padding(.trailing, padding)
                }
            }
        }
    }
}

struct CatView_Previews: PreviewProvider {
    static var previews: some View {
        CatCard(cat: Cat(id: "5",name: "Siamese", origin: "Thailand", affection_level: 5, intelligence: 5,reference_image_id: "xnsqonbjW"))
    }
}

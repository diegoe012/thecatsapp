//
//  ContentView.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var catViewModel: CatViewModel = CatViewModel()
    
    var body: some View {
        ScrollView{
            Section(header: Text("Catbreeds")
                .font(.title)
                .bold()

            ){
                LazyVStack{
                    ForEach(catViewModel.catList){ cat in
                        CatCard(cat: cat)
                        Divider()
                    }
                }
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  TheCatApssApp.swift
//  TheCatApss
//
//  Created by Diegoe012 on 16/05/23.
//

import SwiftUI

@main
struct TheCatApssApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
